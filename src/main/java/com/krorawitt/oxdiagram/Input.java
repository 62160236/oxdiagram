/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.krorawitt.oxdiagram;

/**
 *
 * @author WINDOWS 10
 */
import java.util.Scanner;

public class Input {

    private int count;
    private String OX;
    private Board board;
    private int row;
    private int column;

    public Input(Board g) {
        board = g;
        this.count = 0;
        row = board.row();
        column = board.column();
        OX = "||";

    }

    public void getinput() {
        int[][] Check = {{7, 8, 9}, {4, 5, 6}, {1, 2, 3}};
        if (count % 2 == 0) {
            OX = "O";
            System.out.print("O turn ");
            System.out.println();
            System.out.print("Please input: ");
            System.out.println();
        } else {
            OX = "X";
            System.out.print("X turn ");
            System.out.println();
            System.out.print("Please input: ");
            System.out.println();
        }
        Scanner INp = new Scanner(System.in);

        int h = INp.nextInt();
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if ((h == Check[i][j]) && (board.getboardval(i, j) == "||")) {
                    board.setboard(i, j, OX);
                    count += 1;
                    board.up_count();
                }
            }
        }
    }
}