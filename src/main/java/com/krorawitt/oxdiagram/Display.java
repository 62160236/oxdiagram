/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.krorawitt.oxdiagram;

/**
 *
 * @author WINDOWS 10
 */
public class Display {

    private int row;
    private int column;
    private Board board;

    public Display(Board g) {
        board = g;
        row = board.row();
        column = board.column();
        System.out.println("Welcom To OX Game");
        System.out.println();
        printboard();

    }

    public void printboard() {
        for (int a = 0; a < row; a++) {
            for (int b = 0; b < column; b++) {
                System.out.print("\t" + board.getboardval(a, b));
            }
            System.out.println();
        }

    }

}