/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.krorawitt.oxdiagram;

/**
 *
 * @author WINDOWS 10
 */
import java.util.Scanner;

public class Board {

    private boolean stop;
    private int row;
    private int col;
    private String[][] board;
    private int count;

    public Board(int row, int column) {
        count = 0;
        board = new String[row][column];
        this.row = row;
        col = column;
        stop = false;
        String data = "|_|";
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                board[i][j] = data;
            }
        }
    }

    public String getboardval(int a, int b) {
        return board[a][b];
    }

    public void setboard(int a, int b, String input) {
        board[a][b] = input;
    }

    public void show_win() {
        int C_B1 = 0;
        int C_B2 = 0;
        int countRC = row * col;
        if (count >= 4) {
            for (int i = 0; i < row; i++) {
                int C_C = 0;
                int C_R = 0;
                if ((board[0][0] == board[i][i]) && (board[0][0] != "|_|")) {
                    C_B1 += 1;
                    if (C_B1 == row) {
                        stop = true;
                        System.out.println(board[0][0] + " WIN!!");
                        System.out.println("Bye Bye....");
                    }
                }
                if ((board[0][row - 1] == board[i][row - 1 - i]) && (board[0]
                        [row - 1] != "|_|")) {
                    C_B2 += 1;
                    if (C_B2 == row) {
                        stop = true;
                        System.out.println(board[0][row - 1] + " WIN!!");
                        System.out.println("Bye Bye....");
                    }
                }
                for (int j = col - 1; j > 0; j--) {
                    if ((board[i][0] == board[i][j]) && 
                            (board[i][0] != "|_|")) {
                        C_C += 1;
                        if (C_C == row - 1) {
                            stop = true;
                            System.out.println(board[i][0] + " WIN!!");
                            System.out.println("Bye Bye....");
                        }
                    }
                    if ((board[0][i] == board[j][i]) && 
                            (board[0][i] != "|_|")) {
                        C_R += 1;
                        if (C_R == row - 1) {
                            stop = true;
                            System.out.println(board[0][i].charAt(1) + " WIN!!");
                            System.out.println("Bye Bye....");
                        }
                    }

                }

            }
        }
        if ((count == countRC) && (!stop)) {
            stop = true;
            System.out.println("DRAW!!");
            System.out.println("Bye Bye....");

        }

    }

    public void up_count() {
        count += 1;
    }

    public int row() {
        return row;
    }

    public int column() {
        return col;
    }

    public boolean winganme() {
        return stop;
    }
}